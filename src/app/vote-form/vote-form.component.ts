import {
  HttpClient,
  HttpErrorResponse,
  HttpParams,
} from '@angular/common/http';
import { ArrayType } from '@angular/compiler';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import {
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  FormGroupDirective,
  NgForm,
  Validators,
} from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { MatIconRegistry } from '@angular/material/icon';
import { MatStepper, StepperOrientation } from '@angular/material/stepper';
import { DomSanitizer } from '@angular/platform-browser';
import { EMPTY, Observable, of, Subject } from 'rxjs';
import {
  startWith,
  map,
  filter,
  debounceTime,
  distinctUntilChanged,
  switchMap,
  tap,
  finalize,
  catchError,
} from 'rxjs/operators';
import { VoteFormService } from './vote-form.service';

import { BreakpointObserver } from '@angular/cdk/layout';
import { StepperSelectionEvent } from '@angular/cdk/stepper';

export const stripAccents = (str: string): string => {
  return str.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
};

export const padLeft = (
  text: string,
  padChar: string,
  size: number,
): string => {
  return (String(padChar).repeat(size) + text).substring(size * -1, size);
};

const ucFirst = (
  str: string,
  mode: 'eachWord' | 'firstLetter' | 'firstChar' | 'firstWord' = 'eachWord',
  wordSeparator: RegExp | string = /\s/,
) => {
  const letters = /[A-Za-zÀ-ÖØ-öø-ÿ]/;
  const ws =
    '^|' +
    (wordSeparator instanceof RegExp
      ? '(' + wordSeparator.source + ')'
      : // sanitize string for RegExp https://stackoverflow.com/questions/3446170/escape-string-for-use-in-javascript-regex#comment52837041_6969486
        '[' + wordSeparator.replace(/[[{}()*+?^$|\]\.\\]/g, '\\$&') + ']');

  const r =
    mode === 'firstLetter'
      ? letters
      : mode === 'firstChar'
      ? new RegExp('^' + letters.source)
      : mode === 'firstWord' || mode === 'eachWord'
      ? new RegExp(
          '(' + ws + ')' + letters.source,
          mode === 'eachWord' ? 'g' : undefined,
        )
      : undefined;

  if (r) {
    return str.replace(r, (c) => c.toUpperCase());
  } else {
    throw `error: ucFirst invalid mode (${mode}). Parameter should be one of: firstLetter|firstChar|firstWord|eachWord`;
  }
};

export type Location = any;

export interface Identity {
  location: string | Location;
  name: string;
  firstNames: string[];
  firstNameDisabled: boolean;
  gender: string;
  birthDate: {
    day: string;
    month: string;
    year: string;
  };
  birthDateIncomplete: boolean;
  zip: string;
  nne: string;
}

type Communes = any;

export interface VoteStatus {
  open: boolean;
  started: boolean;
  ended: boolean;
  now: Date;
}

const randomString = () =>
  (Math.random() + 1).toString(36).substring(7) +
  '.' +
  new Date().getTime().toString();

export class NneErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(
    control: FormControl | null,
    form: FormGroupDirective | NgForm | null,
  ): boolean {
    const isSubmitted = form && form.submitted;
    return !!(
      control &&
      control.invalid &&
      (control.dirty || control.touched || isSubmitted)
    );
  }
}

@Component({
  selector: 'app-vote-form',
  templateUrl: './vote-form.component.html',
  styleUrls: ['./vote-form.component.scss'],
})
export class VoteFormComponent implements OnInit {
  constructor(
    private voteFormService: VoteFormService,
    private http: HttpClient,
    private fb: FormBuilder,
    iconRegistry: MatIconRegistry,
    sanitizer: DomSanitizer,
    breakpointObserver: BreakpointObserver,
  ) {
    iconRegistry.addSvgIcon(
      'info',
      sanitizer.bypassSecurityTrustResourceUrl('assets/info.svg'),
    );
    iconRegistry.addSvgIcon(
      'open_in_new',
      sanitizer.bypassSecurityTrustResourceUrl(
        'assets/open_in_new_black_24dp.svg',
      ),
    );

    this.stepperOrientation = breakpointObserver
      .observe('(min-width: 800px)')
      .pipe(map(({ matches }) => (matches ? 'horizontal' : 'vertical')));
  }

  @ViewChild('stepper') private stepper!: MatStepper;

  voteStarts = new Date('2022-01-16T00:00:00+01:00');
  voteEnds = new Date('2022-01-29T23:59:59+01:00');

  forceOpen: boolean = false;

  voteStatus(): VoteStatus {
    const now = new Date();
    const started = now >= this.voteStarts || this.forceOpen;
    const ended = now > this.voteEnds;
    const open = started && !ended;

    return <VoteStatus>{ now, started, ended, open };
  }

  voteStatus$: Observable<VoteStatus> = of(this.voteStatus());

  refresh = setInterval(() => {
    this.voteStatus$ = of(this.voteStatus());
  }, 1000);

  isLinear = true;
  step: FormGroup[] = [];

  readonly emptyIdentity: Identity = {
    location: '',
    name: '',
    firstNameDisabled: false,
    firstNames: [''],
    gender: '',
    birthDateIncomplete: false,
    birthDate: {
      day: '',
      month: '',
      year: '',
    },
    nne: '',
    zip: '',
  };

  identity: Identity = this.emptyIdentity;

  vote: string = '';

  suggestedCities$: Observable<any[]> = of([]);
  suggestedCitiesIsLoading$: Observable<boolean> = of(false);

  genders: string[] = ['Féminin', 'Masculin'];

  candidates = [
    'Frédérique COLOMBET-RICHOU',
    'Gilles CORSO',
    'Thomas FRITZ',
    'Philippe FURLAN',
    'Olivier GARRIVIER',
    'Marc JUTIER',
    'Nicolas DE KEERSMAKER',
    'Sébastien LESEIGNEUR',
    'Claire TOURNERET DE HAAS',
    'Vote Blanc',
  ];

  nneMatcher = new NneErrorStateMatcher();
  zipMatcher = new NneErrorStateMatcher();

  birthDateYearMax = new Date().getFullYear() - 17;

  @Input() inputFirstNames!: ArrayType[];

  firstNamesControl!: FormArray;

  stepperOrientation: Observable<StepperOrientation>;

  showVote = false;

  ngOnInit(): void {
    this.step[-1] = this.fb.group({}); // when vote is closed

    this.step[0] = this.fb.group({});

    this.step[1] = this.fb.group({
      location: ['', Validators.required],
      zip: ['', [Validators.required, Validators.pattern(/^[0-9]{5}$/)]],
      name: ['', Validators.required],
      gender: ['', Validators.required],
      firstNameDisabled: [false],
      // firstName: ['', Validators.required],
      firstNames: this.fb.array([]),
      birthDateIncomplete: [false],
      birthDateDay: ['', Validators.required],
      birthDateMonth: ['', Validators.required],
      birthDateYear: ['', Validators.required],
      nne: ['', [Validators.required, Validators.pattern(/^[0-9]{1,9}$/)]],
    });

    this.step[1].controls.firstNameDisabled.valueChanges
      .pipe(
        map((value) => {
          if (value) {
            if (this.firstNamesControl.controls.length) {
              this.firstNamesControl.controls[0].setValue({ firstName: '' });
              this.firstNamesControl.controls.splice(1);
            }

            this.firstNamesControl.disable();
          } else {
            this.firstNamesControl.enable();
          }
        }),
      )
      .subscribe();

    // https://stackoverflow.com/questions/38007236/how-to-dynamically-add-and-remove-form-fields-in-angular-2
    // this.firstNamesControl = this.step[1].controls['firstNames'];
    // this.firstNamesControl.push(
    //   this.fb.group({ firstName: ['', Validators.required] }),
    // );

    this.firstNamesControl = this.step[1].get('firstNames') as FormArray;

    this.addInputFirstName();
    // this.addInputFirstName();

    // const firstNamesControl = <FormArray>this.step[1].controls['firstNames'];
    // firstNamesControl.push(
    //   this.fb.group({ firstName: ['', Validators.required] }),
    // );

    // this.inputFirstNames.forEach((item) => {
    //   let newGroup = this.fb.group({ firstName: [''] });
    //   firstNamesControl.push(newGroup);
    // });

    this.step[1].controls.birthDateIncomplete.valueChanges
      .pipe(
        map((value) => {
          if (value) {
            this.step[1].controls.birthDateDay.setValue('');
            this.step[1].controls.birthDateDay.disable();
          } else {
            this.step[1].controls.birthDateDay.enable();
          }
        }),
      )
      .subscribe();

    this.step[2] = this.fb.group({});
    this.step[3] = this.fb.group({
      candidate: ['', Validators.required],
    });
    this.step[4] = this.fb.group({});

    this.suggestedCities$ = this.step[1].controls['location'].valueChanges.pipe(
      startWith(''),
      // avoid trying to suggest after selecting on city
      filter((data) => data && !data.id),
      distinctUntilChanged(),
      filter((data) => data.trim().length >= 2),
      tap(() => {
        this.suggestedCitiesIsLoading$ = of(true);
      }),
      debounceTime(300),
      switchMap((citySearch: string) => {
        return this.filteredCities(citySearch);
      }),
      tap(() => {
        this.suggestedCitiesIsLoading$ = of(false);
      }),
    );

    // this.step[1] = this._formBuilder.group({
    //   cityCtrl: ['', Validators.required],
    //   nameCtrl: ['', Validators.required],
    //   zipCtrl: [
    //     '',

    //     Validators.compose([
    //       Validators.required,
    //       Validators.pattern(/^[0-9]{5}$/),
    //     ]),
    //   ],
    //   nneCtrl: [
    //     '',
    //     Validators.compose([
    //       Validators.required,
    //       Validators.pattern(/^[0-9]{9}$/),
    //     ]),
    //   ],
    //   // TODO: FirstNames[]
    //   genderCtrl: ['', Validators.required],
    //   // TODO: conditional
    //   // birthDateDayCtrl: ['', Validators.required],
    //   birthDateMonthCtrl: ['', Validators.required],
    //   birthDateYearCtrl: ['', Validators.required],
    // });
  }

  // get(q: string) {
  //   return this.http.get<any[]>(URL + q).pipe(
  //     // Set loading to true when request begins
  //     tap(() => this.loading.next(true)),
  //     // If we get to this point, we know we got the data,
  //     // set loading to false, return only the items
  //     map((res) => {
  //       this.loading.next(false);
  //       return res.items;
  //     }),
  //   );
  // }

  // locationChange() {
  //   this.step[1].controls.zip.setValue('toto');

  //   // console.log(this.step[1].value.location);
  //   if (this.step[1].value.location?.cp && this.step[1].value.zip != '') {
  //     this.step[1].controls.zip.setValue(this.step[1].value.location.cp);
  //   }
  // }

  locationChange(selected: any) {
    // console.log('selected', selected);
    if (
      this.step[1].value.location?.cp
      // &&
      // (!this.step[1].value.zip || this.step[1].value.zip == '')
    ) {
      this.step[1].controls.zip.setValue(
        this.step[1].value.location.cp.slice(0, 5),
      );
    }
  }

  searchCities({
    q = undefined,
    limit = undefined,
  }: {
    q: string | undefined;
    limit: number | undefined;
  }): Observable<any[]> {
    let params = new HttpParams();

    if (q !== undefined) {
      params = params.append('q', q);
    }

    if (limit !== undefined) {
      params = params.append('limit', limit);
    }

    // const API_BASE = `https://public.opendatasoft.com/api/records/1.0/search/`;

    // params.append('dataset', 'correspondance-code-insee-code-postal');
    // params.append('facet', 'insee_com');
    // params.append('facet', 'nom_dept');
    // params.append('facet', 'nom_region');
    // params.append('facet', 'statut');

    const API_COMMUNES = `https://api.laprimairefr.org/communes`;

    try {
      // this.suggestedCitiesIsLoading$ = of(true);

      return this.http
        .get<any>(API_COMMUNES, {
          params: params,
        })
        .pipe(
          map((res) => {
            // this.suggestedCitiesIsLoading$ = of(false);

            return res;
          }),
          // tap(() => (this.suggestedCitiesIsLoading$ = of(false))),
          catchError((error: HttpErrorResponse) => {
            if (error.error instanceof Error) {
              // A client-side or network error occurred. Handle it accordingly.
              console.error('An error occurred:', error.error.message);
            } else {
              // The backend returned an unsuccessful response code.
              // The response body may contain clues as to what went wrong,
              console.error(
                `Backend returned code ${error.status}, body was: ${error.error}`,
              );
            }

            return EMPTY;
          }),
        );
    } catch (e) {
      console.error(e);
    }

    return of([]);

    // return this.http.get<any>(`https://geo.api.gouv.fr/communes`, {
    //   params: params,
    // });
  }

  filteredCities(q: string, limit: number = 30): Observable<any[]> {
    if (q && String(q).trim().length >= 2) {
      return this.searchCities({
        q,
        limit,
      }).pipe(
        // city.nom == citySearch => first in list
        map((data) => {
          data.sort((a: any, b: any) => {
            const qClean = stripAccents(q.toUpperCase()).replace('/s+/g', ' ');
            let aNomClean = a.NCC;
            let bNomClean = b.NCC;

            return aNomClean == qClean ? 1 : bNomClean == qClean ? -1 : 0;
          });

          return data;
        }),
      );
    }

    return of([]);
  }

  cityControlDisplayName(city: Communes): string {
    if (!city || !city.LIBELLE) {
      return '';
    }

    let name: string = '';

    name = city.LIBELLE;

    if (city.DEP) {
      name += ` ${city.DEP}`;
    }

    return name;
  }

  // firstNameDisabledToggle() {
  //   if (this.identity.firstNameDisabled) {
  //     this.firstNames = [{ value: '' }];
  //   }
  // }

  // firstNameRemove(i: number) {
  //   this.firstNames.splice(i, 1);
  // }

  // firstNameAdd() {
  //   this.firstNames.push({
  //     value: '',
  //   });
  // }

  resetForm() {
    this.step.forEach((s) => s.reset());

    this.updateIdentity();
    this.updateVote();

    console.log(`All form data has been wiped.`);
  }

  stepperSelectionChange(event: StepperSelectionEvent) {
    if (event.selectedIndex > 1) {
      this.updateIdentity();
      this.updateVote();
    }
  }

  resetStepper() {
    this.resetForm();
    this.resetSubmitObservers();
    this.stepper.reset();
  }

  confirmResetStepper() {
    if (
      !this.step[1].touched ||
      confirm('Êtes vous certain de vouloir perdre toutes vos saisies ?')
    ) {
      this.resetStepper();
    }
  }

  consoleLog(data: any) {
    this.updateIdentity();
    console.log(data);
    // console.log('step[1].value', this.step[1].value);
  }

  updateIdentity() {
    const {
      location,
      zip,
      name,
      firstNameDisabled,
      firstNames,
      gender,
      birthDateIncomplete,
      birthDateDay,
      birthDateMonth,
      birthDateYear,
      nne,
    } = this.step[1].value;

    if (location === null) {
      this.identity = this.emptyIdentity;
    } else {
      this.identity = {
        location:
          location && location.id
            ? this.cityControlDisplayName(location)
            : String(location || '').trim(),
        zip: (zip || '').trim(),
        name: (name || '').trim(),
        firstNameDisabled: firstNameDisabled,
        firstNames: firstNames
          ? firstNames.map((item: any) => item.firstName.trim())
          : [''],
        gender: gender,
        birthDateIncomplete: birthDateIncomplete,
        birthDate: {
          day: birthDateDay,
          month: birthDateMonth,
          year: birthDateYear,
        },
        nne: nne,
      };
    }

    console.log('Updated Identity');
  }

  updateVote() {
    this.vote = this.step[3].value.candidate;

    console.log('Updated candidate');
  }

  voteSubmitLoading$ = of(false);
  voteSubmitLock$ = of(false);
  voteSubmitSucceeded$ = of(false);
  voteSubmitErrorHasVoted$ = of(false);

  resetSubmitObservers() {
    this.voteSubmitLoading$ = of(false);
    this.voteSubmitLock$ = of(false);
    this.voteSubmitSucceeded$ = of(false);
    this.voteSubmitErrorHasVoted$ = of(false);
  }

  voteSubmit() {
    const API_VOTE_REGISTER = `https://api.laprimairefr.org/votes/register`;
    // const API_VOTE_REGISTER = `http://localhost:3000/votes/register`;

    this.voteSubmitLoading$ = of(true);

    this.http
      .post(API_VOTE_REGISTER, {
        identity: this.identity,
        candidate: this.vote,
      })
      .pipe(
        tap(() => {
          this.voteSubmitLoading$ = of(false);
        }),
      )
      .subscribe(
        (response: any) => {
          // console.log(response);

          if (response.status == 'success') {
            this.voteSubmitLock$ = of(true);
            this.voteSubmitSucceeded$ = of(true);
            this.resetForm();
            return;
          }
          if (response.status == 'failedHasVoted') {
            this.voteSubmitLock$ = of(true);
            this.voteSubmitSucceeded$ = of(false);
            this.voteSubmitErrorHasVoted$ = of(true);
            this.resetForm();
            return;
          }
        },
        (error) => console.error(error),
      );
  }

  formatDate(n: string | number) {
    if (n === '' || n === null || n === undefined) {
      return '';
    }

    n = n.toString();

    return n.length === 1 && n !== '0' ? ('0' + n).slice(-2) : n.slice(-2);
  }

  trim(str: string) {
    return str ? str.toString().trim() : str;
  }

  // untraceableFieldName: string = 'locatorId' + randomString();
  // fixChromeAutocomplete(): void {
  //   this.untraceableFieldName = 'locatorId' + randomString();
  // }

  ucFirst = ucFirst;

  addInputFirstName(): void {
    this.firstNamesControl.push(
      this.fb.group({
        firstName: ['', Validators.required],
      }),
    );
  }

  delInputFirstName(index: number): void {
    this.firstNamesControl.removeAt(index);
  }

  forceOpenToggle() {
    this.forceOpen = !this.forceOpen;
    // this.voteStatus$
    //   .pipe(
    //     map((value) => {
    //       // const {started, ended, open} = this.voteStatus()
    //       this.voteStatus$ = of({
    //         started: value.started,
    //         ended: value.ended,
    //         open: !value.open,
    //         now: value.now,
    //       });
    //       return !value;
    //     }),
    //   )
    //   .subscribe();
  }

  now() {
    return new Date();
  }

  dateDiff(startDate: Date, endDate: Date = new Date()) {
    var diff = endDate.getTime() - startDate.getTime();
    var days = Math.floor(diff / (60 * 60 * 24 * 1000));
    var hours = Math.floor(diff / (60 * 60 * 1000)) - days * 24;
    var minutes =
      Math.floor(diff / (60 * 1000)) - (days * 24 * 60 + hours * 60);
    var seconds =
      Math.floor(diff / 1000) -
      (days * 24 * 60 * 60 + hours * 60 * 60 + minutes * 60);
    return { day: days, hour: hours, minute: minutes, second: seconds };
  }
}
