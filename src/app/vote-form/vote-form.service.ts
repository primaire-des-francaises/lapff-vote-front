import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class VoteFormService {
  constructor(private http: HttpClient) {}

  searchCities({
    q = undefined,
    limit = undefined,
  }: {
    q: string | undefined;
    limit: number | undefined;
  }): Observable<any[]> {
    let params = new HttpParams();

    if (q !== undefined) {
      params = params.append('q', q);
    }

    if (limit !== undefined) {
      params = params.append('limit', limit);
    }

    try {
      return this.http.get<any>(`https://api.laprimairefr.org/communes`, {
        params: params,
      });
    } catch (e) {
      console.error(e);
    }

    return of([]);

    // return this.http.get<any>(`https://geo.api.gouv.fr/communes`, {
    //   params: params,
    // });
  }
}
